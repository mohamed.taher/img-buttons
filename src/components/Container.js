import React, { useEffect, useState } from 'react';
import {useInterval} from 'react-interval-hook'
import Img from "../components/Img"
import Btn from "./Btn"
import "../App.css"


const Container = () => {

  let [currentImg, setCurrentImg] = useState(imgsArr[0])

  let whenInterval = () => {
    let currentSelectedInd = imgsArr
    .findIndex(img => img == currentImg)

    if (currentSelectedInd == imgsArr.length-1)
      currentSelectedInd = -1

    setCurrentImg(imgsArr[currentSelectedInd + 1])
  }
  
  let { start, stop, isActive } = useInterval(
    whenInterval, 1000, { autoStart: false }
  )

  let btnClickHandler = (e) => {
    if(isActive()) stop()
    
    let name = e.target.name
    let ind = parseInt(name.split("btn")[1]) - 1
    setCurrentImg(imgsArr[ind])
  }
  
  let checkAct = (name) => {
    let thisInd = parseInt(name.split("btn")[1]) - 1,
      currentSelectedInd = imgsArr.findIndex(img => img == currentImg)

    return (thisInd == currentSelectedInd) ? "btnAct" : "btnDisact"
  }


  let imgClickHandler = () => {
    if (!isActive()) start()
    else stop()
  }

  return (
    <div className="container">
      <Btn name="btn1" className={`btn btn1 ${checkAct("btn1")}`} onClick={btnClickHandler} />
      <Btn name="btn2" className={`btn btn2 ${checkAct("btn2")}`} onClick={btnClickHandler} />
      <Btn name="btn3" className={`btn btn3 ${checkAct("btn3")}`} onClick={btnClickHandler} />
      <Img src={currentImg} className="img" onClick={imgClickHandler} />
    </div>
  );
}

let imgsArr = [
  "https://image.shutterstock.com/image-photo/spring-blossom-background-beautiful-nature-260nw-1033292395.jpg",
  "https://static.scientificamerican.com/sciam/cache/file/4E0744CD-793A-4EF8-B550B54F7F2C4406_source.jpg?w=590&h=800&ACB6A419-81EB-4B9C-B846FD8EBFB16FBE",
  "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQ7jwsUNguvuysHDWl-4M0v_oqDkiWSvCSUTA&usqp=CAU"
]



export default Container