import React, { useState, useEffect } from 'react';
import "../App.css"

const Container = ({
  onClick = () =>{},
  className="",
  src=""
}) => {

    return (
      <img className={className} src={src} onClick={onClick} />
    );
}

export default Container