import React, { Component } from 'react';
import "../App.css"

const Btn = ({
  onClick = () => {},
  className = "",
  name=""
}) => {

    return (
        <button
        onClick={onClick}
        className={className}
        name={name}
        >
          {name}
        </button>
    );
}

export default Btn